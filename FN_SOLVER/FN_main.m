% Main - Numerical approximation of the full-order model for an instance of the parameter epsilon

%   Copyright (c) 2018, Politecnico di Milano 
%   LocalROM - Stefano Pagani <stefano.pagani at polimi.it>

clc
clear all
close all

% model parameters definition
param(1) = 1;      % domain lenght
param(2) = 0.015;  % conducibility
param(3) = 0.5;    % recovery parameter
param(4) = 2;      % recovery parameter

% constructor
FNS = FNSolver(param, 256, 0, 2, 400);
[u,w] = FNS.solveFOM(0.005);
U=[u',w'];

% solve the forward model
for i=1:10
    
     [u,w] = FNS.solveFOM(0.005*i);   
     
     U=[U,u',w'];
     
    
end




