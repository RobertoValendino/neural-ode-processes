# Neural ODE processes
Our code relies on a great extent of the official code for the paper Neural ODE Processes (ICLR 2021) by Cristian Bodnar (https://github.com/crisbodnar/ndp). In particular we have added new datasets, for example the Gauss, the Lorentz, the 3D Lotka-Volterra and the eye system datasets. We have also implemented a new NN, the so called mynn and we have tested it on all these datasets and we have compared its behavior with the ones of the already implemented neural networks. Another dataset that we have implemented is based on the solution of the monodomain equation coupled with the FitzHugh-Nagumo cellular model.

# Getting started and running the experiments
For development, we have used Python 3.8.5. To run the experiments you have to save all the content of this repository on your Google drive and run training_main.ipynb on Google Colab. In particular the file in ndp-main are related to the training of the neural network while the file in FN_SOLVER are used to built the monodomain equation dataset.

# Credits 
We have implemented the dataset based on the pde solution using the code LocalROM by Stefano Pagani
(https://github.com/StefanoPagani/LocalROM).
